/**
 * @file   test_drvTest.c
 * @author FSA
 * @date   25.04.2022
 * @version 0.1
 * @brief  A Linux user space program that communicates with the drvTest driver.
 * It passes a string to the driver and reads the response from it.
 * For this example to work the device must be called /dev/drvTest
*/

#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

#define BUFFER_LENGTH (8 + 100*8)               // The buffer length 
static unsigned char buffer[BUFFER_LENGTH];     // The receive buffer from the driver

int main()
{
    int ret, fd;

    printf("Starting device test code example...\n");
    fd = open("/dev/drvTest", O_RDONLY);             // Open the device with read-only access
    if (fd < 0)
    {
        perror("Failed to open the device...");
        return errno;
    }

    printf("Reading from the device...\n");
    ret = read(fd, buffer, BUFFER_LENGTH);        // Read the response from the driver
    if (ret < 0){
        perror("Failed to read the message from the device.");
        return errno;
    }
    printf("Message size: %llu bytes (expected: 808 bytes)\n", sizeof(buffer));

    uint64_t counter =
    ((uint64_t)buffer[0] <<  0) +
    ((uint64_t)buffer[1] <<  8) +
    ((uint64_t)buffer[2] << 16) +
    ((uint64_t)buffer[3] << 24) +
    ((uint64_t)buffer[4] << 32) +
    ((uint64_t)buffer[5] << 40) +
    ((uint64_t)buffer[6] << 48) +
    ((uint64_t)buffer[7] << 56);
    
    printf("Counter = %llu\n\n", counter);
    
    for (size_t i = 0; i < 100; i++) {
    
        size_t index = 8 + 8*i;
        
        uint16_t r  = ((uint16_t)buffer[index + 0] << 0) + ((uint16_t)buffer[index + 1] << 8);
        uint16_t g  = ((uint16_t)buffer[index + 2] << 0) + ((uint16_t)buffer[index + 3] << 8);
        uint16_t b  = ((uint16_t)buffer[index + 4] << 0) + ((uint16_t)buffer[index + 5] << 8);
        uint16_t ir = ((uint16_t)buffer[index + 6] << 0) + ((uint16_t)buffer[index + 7] << 8);

        if (r == 0xFFFF || g == 0xFFFF || b == 0xFFFF || ir == 0xFFFF) {
            printf("Color RGBI [%llu]: not set\n", i);
        }
        else {
            printf("Color RGBI [%llu]: %u, %u, %u, %u\n", i, r, g, b, ir);
        }
    }
    
    printf("\n[");
    for (size_t i = 0; i < BUFFER_LENGTH; i++) {
        printf("%d,", buffer[i]);
    }
    
    printf("]\n\nEnd of the program\n");
    return 0;
}

