/***************************************************************************//**
*  \file       drvTestI2C.c
*
*  \details    Simple I2C driver explanation
*
*  \author     FSA
*
* *******************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/fs.h>           /* Header for the Linux file system support 	*/
#include <linux/uaccess.h>      /* Required for the copy to user function		*/
#include <linux/moduleparam.h> 	/* Needed for module parameters 				*/
#include <linux/device.h>       /* Header to support the kernel Driver Model	*/
#include <linux/kthread.h>

#define I2C_BUS_AVAILABLE   (          1 )              // I2C Bus available in our Raspberry Pi
#define SLAVE_DEVICE_NAME   ( "TCS34725" )              // Device and Driver Name
#define TCS_SLAVE_ADDR      (       0x29 )              // TCS34725 Slave Address

#define DEVICE_NAME "drvTest" /* The device will appear at /dev/drvTest using this value	*/
#define CLASS_NAME  "hearc"   /* The device class -- this is a character device driver	*/

static int majorNumber;                 	/* Device number -- determined automatically			*/

static struct class*  devTestClass  = NULL; /* The device-driver class struct pointer				*/
static struct device* devTestDevice = NULL; /* The device-driver device struct pointer				*/

static struct i2c_adapter *tcs_i2c_adapter = NULL;  	// I2C Adapter Structure
static struct i2c_client  *tcs_i2c_client  = NULL;  	// I2C Cient Structure

typedef struct {
    uint8_t r_msb;
    uint8_t r_lsb;
    uint8_t g_msb;
    uint8_t g_lsb;
    uint8_t b_msb;
    uint8_t b_lsb;
    uint8_t i_msb;
    uint8_t i_lsb;
} rgbi_t;

enum { NB_COLORS = 100 };
static rgbi_t colors[NB_COLORS];
static size_t color_offset = 0;
static uint64_t counter = 0;

static ssize_t dev_read(struct file *, char *, size_t, loff_t *);

static struct file_operations fops =
{
   //.open = dev_open,
   .read = dev_read,
   //.write = dev_write,
   //.release = dev_release,
};

/*
** This function writes the data into the I2C client
**
**  Arguments:
**      buff -> buffer to be sent
**      len  -> Length of the data
**
*/
static int I2C_Write(unsigned char *buf, unsigned int len)
{
    /*
    ** Sending Start condition, Slave address with R/W bit,
    ** ACK/NACK and Stop condtions will be handled internally.
    */
    int ret = i2c_master_send(tcs_i2c_client, buf, len);

    return ret;
}

/*
** This function reads one byte of the data from the I2C client
**
**  Arguments:
**      out_buff -> buffer wherer the data to be copied
**      len      -> Length of the data to be read
**
*/
static int I2C_Read(unsigned char *out_buf, unsigned int len)
{
    /*
    ** Sending Start condition, Slave address with R/W bit,
    ** ACK/NACK and Stop condtions will be handled internally.
    */
    int ret = i2c_master_recv(tcs_i2c_client, out_buf, len);

    return ret;
}

/*
** This function sends the command/data to the TCS.
**
**  Arguments:
**      data   -> data to be written
**
*/
static void TCS_Write(unsigned char * data)
{
    int ret;

    ret = I2C_Write(data, 2);
}

/*
** This function reads the data to the TCS.
**
**  Arguments:
**      data   -> data to be read
**
*/
static void TCS_Read(unsigned char * data)
{
    int ret;
  	// Read 8 bytes of data from register(0x94)
	// cData lsb, cData msb, red lsb, red msb, green lsb, green msb, blue lsb, blue msb
	char reg[1] = {0x94};

	ret = I2C_Write(reg, 1);
    ret = I2C_Read(data, 8);
}

/*
** This function sends the commands that need to used to Initialize the TCS.
**
**  Arguments:
**      none
**
*/

static int TCS_init(void)
{

    // Set all values in the buffer to 0xFF
    memset(colors, 0xFF, sizeof(colors));
    
    return 0;
}

static int update_buffer(void* arg)
{
    uint8_t config[2] = {0};
    uint8_t data[8] = {0};
    
    while (1)
    {
        //
        // Commands to initialize the TCS
        //

        // Select enable register(0x80)
	    // Power ON, RGBC enable, wait time disable(0x03)
	    config[0] = 0x80;
	    config[1] = 0x03;
	    TCS_Write(config);
	    // Select ALS time register(0x81)
	    // Atime = 700 ms(0x00)
	    config[0] = 0x81;
	    config[1] = 0x00;
	    TCS_Write(config);
	    // Select Wait Time register(0x83)
	    // WTIME : 2.4ms(0xFF)
	    config[0] = 0x83;
	    config[1] = 0xFF;
	    TCS_Write(config);
	    // Select control register(0x8F)
	    // AGAIN = 1x(0x00)
	    config[0] = 0x8F;
	    config[1] = 0x00;
	    TCS_Write(config);
	    msleep(10);

	    TCS_Read(data);
	    
	    // Convert the data
	    {
	        rgbi_t* rgbi = (rgbi_t*)(colors + color_offset);
	        
	        rgbi->r_msb = data[1];
	        rgbi->r_lsb = data[0];
	        rgbi->g_msb = data[3];
	        rgbi->g_lsb = data[2];
	        rgbi->b_msb = data[5];
	        rgbi->b_lsb = data[4];
	        rgbi->i_msb = data[7];
	        rgbi->i_lsb = data[6];
	    }
	    
	    color_offset = (color_offset + 1) % NB_COLORS;
	    counter++;
	    
        msleep(100); // delay
    }
    
    return 0;
}

/*
** This function getting called when the slave has been found
** Note : This will be called only once when we load the driver.
*/
static int tcs_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    pr_info("TCS Probed!!!\n");
    return 0;
}

/*
** This function getting called when the slave has been removed
** Note : This will be called only once when we unload the driver.
*/
static int tcs_remove(struct i2c_client *client)
{
    pr_info("TCS Removed!!!\n");
    return 0;
}

/*
** Structure that has slave device id
*/
static const struct i2c_device_id tcs_id[] = {
        { SLAVE_DEVICE_NAME, 0 },
        { }
};
MODULE_DEVICE_TABLE(i2c, tcs_id);

/*
** I2C driver Structure that has to be added to linux
*/
static struct i2c_driver tcs_driver = {
        .driver = {
            .name   = SLAVE_DEVICE_NAME,
            .owner  = THIS_MODULE,
        },
        .probe          = tcs_probe,
        .remove         = tcs_remove,
        .id_table       = tcs_id,
};

/*
** I2C Board Info strucutre
*/
static struct i2c_board_info tcs_i2c_board_info = {
        I2C_BOARD_INFO(SLAVE_DEVICE_NAME, TCS_SLAVE_ADDR)
    };

/*
** Module Init function
*/
static int __init tcs_driver_init(void)
{
    int ret = -1;
    tcs_i2c_adapter     = i2c_get_adapter(I2C_BUS_AVAILABLE);

    pr_info("devTest: Initializing the devTest driver\n");

   // Try to dynamically allocate a major number for the device
   majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
   if (majorNumber<0)
   {
      pr_alert("devTest: failed to register a major number\n");
      return majorNumber;
   }
   pr_info("devTest: registered (major number %d)\n", majorNumber);

   // Register the device class
   devTestClass = class_create(THIS_MODULE, CLASS_NAME);
   if (IS_ERR(devTestClass))
   {  // Check for error and clean up if there is
      unregister_chrdev(majorNumber, DEVICE_NAME);
      pr_alert("Failed to register device class\n");
      return PTR_ERR(devTestClass);  // Return an error on a pointer
   }
   pr_info("devTest: device class registered\n");

   // Register the device driver
   devTestDevice = device_create(devTestClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
   if (IS_ERR(devTestDevice))
   {  // Clean up if there is an error
      class_destroy(devTestClass);
      unregister_chrdev(majorNumber, DEVICE_NAME);
      pr_alert("Failed to create the device\n");
      return PTR_ERR(devTestDevice);
   }
   pr_info("devTest: device class created\n");

    if( tcs_i2c_adapter != NULL )
    {
        tcs_i2c_client = i2c_new_client_device(tcs_i2c_adapter, &tcs_i2c_board_info);
        if( tcs_i2c_client != NULL )
        {
            i2c_add_driver(&tcs_driver);
            ret = 0;
        }

        i2c_put_adapter(tcs_i2c_adapter);
    }
    
    TCS_init();
    
    // Thread
    {
        struct task_struct* thread;
        
        thread = kthread_run(update_buffer, NULL, "tbuf");
        
        if (IS_ERR(thread)) {
            int err = PTR_ERR(thread);
            pr_alert("Failed to create thread\n");
            thread = NULL;
            return err;
        }
    }

    pr_info("Driver Added!!!\n");
    return ret;
}

/*
** Module Exit function
*/
static void __exit tcs_driver_exit(void)
{
   device_destroy(devTestClass, MKDEV(majorNumber, 0)); 	// remove the device
   class_unregister(devTestClass);                          // unregister the device class
   class_destroy(devTestClass);                             // remove the device class
   unregister_chrdev(majorNumber, DEVICE_NAME);             // unregister the major number
   pr_info("devTest: Goodbye!\n");

    i2c_unregister_device(tcs_i2c_client);
    i2c_del_driver(&tcs_driver);
    pr_info("Driver Removed!!!\n");
}

static ssize_t dev_read(struct file *filep, char *buf_dst, size_t len, loff_t *offset)
{
    enum {
        BUFFER_SIZE = sizeof(uint64_t) + sizeof(colors)
    };
    
    static uint8_t buffer[BUFFER_SIZE];
    int error_count = 0;
    
    buffer[0] = (counter >>  0) & 0xFF;
    buffer[1] = (counter >>  8) & 0xFF;
    buffer[2] = (counter >> 16) & 0xFF;
    buffer[3] = (counter >> 24) & 0xFF;
    buffer[4] = (counter >> 32) & 0xFF;
    buffer[5] = (counter >> 40) & 0xFF;
    buffer[6] = (counter >> 48) & 0xFF;
    buffer[7] = (counter >> 56) & 0xFF;
    
    // Copy data from the ring buffer (colors) to the output buffer with correct order
    memcpy(buffer + sizeof(uint64_t) + sizeof(rgbi_t) * color_offset, colors, sizeof(rgbi_t) * color_offset);
    memcpy(buffer + sizeof(uint64_t), colors + color_offset, sizeof(rgbi_t) * (NB_COLORS - color_offset));
    
    // copy_to_user has the format ( * to, *from, size) and returns 0 on success
    error_count = copy_to_user(buf_dst, buffer, BUFFER_SIZE);

    if (error_count==0)
    {            // if true then have success
        pr_info("devTest: Sent %d characters to the user\n", BUFFER_SIZE);
        return 0;  // clear the position to the start and return 0
    }
    else
    {
        pr_info("devTest: Failed to send %d characters to the user\n", error_count);
        return -EFAULT;          // Failed -- return a bad address message (i.e. -14)
    }
}

module_init(tcs_driver_init);
module_exit(tcs_driver_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("FSA <blabla@gmail.com>");
MODULE_DESCRIPTION("Simple I2C driver for TCS34725");
MODULE_VERSION("1.0");

