import os
import random as rnd

# Convert u16 to bin
def u16ToBin(u16):

    ret = bytearray()

    ret.append((u16 >> 0) & 0xFF)
    ret.append((u16 >> 8) & 0xFF)

    return ret

# Convert u32 to bin
def u32ToBin(u32):

    ret = bytearray()

    ret.append((u32 >> 0) & 0xFF)
    ret.append((u32 >> 8) & 0xFF)
    ret.append((u32 >> 16) & 0xFF)
    ret.append((u32 >> 24) & 0xFF)

    return ret

# Convert u64 to bin
def u64ToBin(u64):

    ret = bytearray()

    ret.append((u64 >> 0) & 0xFF)
    ret.append((u64 >> 8) & 0xFF)
    ret.append((u64 >> 16) & 0xFF)
    ret.append((u64 >> 24) & 0xFF)
    ret.append((u64 >> 32) & 0xFF)
    ret.append((u64 >> 40) & 0xFF)
    ret.append((u64 >> 48) & 0xFF)
    ret.append((u64 >> 56) & 0xFF)

    return ret



counter = 0

for file_index in range(20):

    buf = bytearray()

    buf += u64ToBin(1 + counter)

    for i in range(100):

        if i + counter < 99:
            buf += bytearray([0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF])
        else:
            buf += u16ToBin(1000 + i - 99 + counter)
            buf += u16ToBin(2000 + i - 99 + counter)
            buf += u16ToBin(3000 + i - 99 + counter)
            buf += u16ToBin(4000 + i - 99 + counter)
    
    counter += rnd.randint(1, 20)

    filename = './drvTest' + str(file_index)

    if os.path.exists(filename):
        os.remove(filename)

    with open(filename, 'xb') as file:

        file.write(buf)
