# Projet Linux Embarqué
**Groupe 11**

**Membres**
- Allemann Jonas
- Chappuis Sébastien
- Stouder Xavier

# Matériel

Raspberry PI modèle 4B

Module Sense Hat B

VM Linux (faisant suite aux labos du cours)

# Structure du dépôt

**Exemple**
- Son présent des exemples de driver, et d'application réseau en Qt

**doc**
- Contient la marche à suivre du projet (le suivi pour le repropduire)

**driver**
- Contient les fichiers du driver. La structure est la même que sur la VM (les fichiers sont a placer dans le dossier `~/LinEmb/`)

**gen-pseudo-data**
- Contient un script python pour générer des fichiers "pseudo" données. Lire ces fichiers permet d'obtenir le même type de donnée que la lecture du driver.
- Contient également ces fichiers "pseudo" données.

**qtApp**
- Contient les applications Qt du projet :
  - server : Destiné à tourné sur le Raspberry (Qt 5 ou 6)<br>
  Son rôle est de lire périodiquement le driver, et de transmettre les données aux clients connectés.
  - client : destiné à tourner sur n'importe quel type de machine (Qt 5)<br>
  Son rôle est d'afficher les données obtenu une fois connecté au serveur. Les données sont affichées sous forme de graphes.
