**Marche à suivre du projet**
> Cette marche à suivre fait suite aux labos faits durant le cours. Les modifications faites durant les labos ne sont pas indiquées dans ce document, mais sont requises.

**Membres du groupe & tâche effectuée**
- Allemann Jonas (Serveur)
- Chappuis Sébastien (Driver)
- Stouder Xavier (Client)

# Integration du driver de test sur le raspberry
> Produit les fichiers comme dans **doc/Fichiers drvTestI2C**

**drvTestI2C**




```bash
cd ~/LinEmb
mkdir src_drvTestI2C
cd src_drvTestI2C
```

1. Ajouter le .C et le Makefile
(dans le .C, il faudra que les informations de versions soit les mêmes que dans le ".mk" du point 3.)
![](memes%20version%20dans%20.c%20que%20.mk.PNG)

2. Depuis le dossier `src_drvTestI2C` :

```bash
cd ../pi4-config/package
mkdir drvTestI2C
cd drvTestI2C
```

![](drvTesti2C%20fichier%20a%20creer.PNG)

3. et éditer ces fichiers comme requis (voir les fichiers `doc\Fichiers drvTestI2C\pi4-config\package\drvTestI2C`)

4. Ajouter le driver au menuconfig :
```bash
cd ~/LinEmb/pi4-config
gedit Config.in
```

y ajouter notre driver

```bash
source "$BR2_EXTERNAL_PI4_CONFIG_PATH/package/drvTestI2C/Config.in"

cd ~/LinEmb/build-pi4/

make menuconfig
-> External options (tout en bas)  
--> Custom package  
---> Ajouter notre driver  
exit
```

5. Compiler la carte

```bash
make -j4
```

En cas d'erreur : `build-pi4/build/` supprimer notre dossier "ex drvTestI2C[Eventuel version]" et refaire le make

### Verifier que le driver fonctionne
Une fois la carte SD prête -> mettre sur le raspberry et effectuer les choses suivantes :

1. Verifier la présence de notre module
```bash
ls /lib/modules/5.10.92-v7l/extra/
    output : drvTestI2C.ko
```
2. Charger le module
```bash
modprobe drvTestI2C
```
3. Verifier qu'il est executé
```bash
lsmod
    output : drvTestI2C             16384  0
```
4. Verifier qu'il fonctionne
```bash
dmesg
    output (vers la fin) : 
        [  764.515437] Red color luminance : 0 lux
        [  764.520416] Green color luminance : 0 lux
        [  764.525572] Blue color luminance : 0 lux
        [  764.530598] IR  luminance : 0 lux
        [  764.534987] Driver Added!!!
```
5. Si les valeurs sont à 0, il faut recharger le driver
```bash
modprobe -r drvTestI2C
modprobe drvTestI2C
dmesg

    output (vers la fin) :
        [  807.295437] Red color luminance : 1858 lux
        [  807.300470] Green color luminance : 1809 lux
        [  807.305694] Blue color luminance : 1467 lux
        [  807.310771] IR  luminance : 4339 lux
        [  807.315246] Driver Added!!!
```


## Voir nos drivers / modules sur le RPI

```bash
ls /lib/modules/5.10.92-v7l/extra/

    output : displayInfo.ko  drvTestI2C.ko
```

# Changement du driver : ne pas refaire la carte SD
> :memo: Dans notre cas, le nom du driver est **drvTestI2C**

1. générer un .ko (sera placé dans `build-pi4/build/drvTestI2C`) :

```bash
cd ~/LinEmb/build-pi4
make drvTestI2C-rebuild # (Pas d'espace entre le nom et le rebuild)
```

2. déplacer le .ko avec SCP sur le RPI (/lib/modules/[version linux]/extra/) (En partant de `LinEmb`) :

```bash
scp ./build-pi4/build/drvTestI2C-1.0/drvTestI2C.ko root@157.26.91.83:/lib/modules/5.10.92-v7l/extra
```

3. Sur le raspberry, recharger le driver :
```bash
modprobe -r drvTestI2C	// Pour le retirer, ici 
modprobe drvTestI2C // Pour le recharger
```

Par exemple, depuis le dossier `LinEmb` :
```bash
DRV_NAME=drvTestI2C
DRV_FULL_NAME=drvTestI2C-1.0

cd build-pi4
rm -rf build/$DRV_FULL_NAME
make $DRV_NAME-rebuild
cd ..

scp ./build-pi4/build/$DRV_FULL_NAME/$DRV_NAME.ko root@157.26.91.83:/lib/modules/5.10.92-v7l/extra
```

# Compiler un programme pour PI

Par exemple, en compilant un fichier `test_drvTest.c` et en transférant l'exécutable sur le PI :
```bash
C_FILE_NAME=test_drvTest.c
O_FILE_NAME=tdrvt

cd test-drv-test

arm-linux-gnueabihf-gcc $C_FILE_NAME -static -o $O_FILE_NAME &&
scp ./$O_FILE_NAME root@157.26.91.83:/root/$O_FILE_NAME

cd ..
```


# Exemple pour envoyer une application QT vers le RPI

```bash
cd ~/LinEmb/qt/MonProjet/ # Chemin vers le projet QT (contenant le fichier .pro)

# Creation du makefile
~/LinEmb/build-pi4/host/bin/qmake MonProjet.pro

# Compilation
make

#Envoie sur le RPI
ssh root@157.26.91.83 "mkdir -p /home/rpi/apps"
scp ./MonProjetCompiler root@157.26.91.83:/home/rpi/apps/

# Executer le programme sur le RPI
cd /home/rpi/apps
./MonProjetCompiler -platform vnc -geometry 500x500 -plugin evdevmouse &
```

Sur la VM : Ouvrir l'interface graphique via "Remmina"

![Connexion au RPI par Remmina](./Voir%20interface%20QT%20du%20programme%20sur%20RPI%20depuis%20VM.PNG)

# Déployer et exécuter le serveur sur le RPI

Le programme du serveur se trouve dans le dossier `qtApp\server`.

Pour déployer le serveur le raspberry, il est nécessaire de le faire depuis la VM.

Tout d'abord, il faut s'assurer que le serveur est configuré pour le raspberry, dans le fichier d'entête "server.h" :
-  Il faut que les *define* suivants soient commentés. Ils sont en lignes 4 et 5.
- Le 1er permet de compiler le programme avec QT6, sauf que le raspberry possède QT5.
- Le second va indiquer qu'il faut utiliser les fichiers de tests de driver (plutôt que lire le driver).

```c++
//#define COMPILE_QT6
//#define USE_TEST_FILE // Penser a changer le chemin absolu du dossier de test
```

Ensuite, il faut suivre la marche à suivre `Exemple pour envoyer une application QT vers le RPI`. Les commandes si après ont été adaptées pour l'envoi du serveur (en supposant que le dossier du serveur se trouve dans `~/LinEmb/qtApp/`):

```bash
cd ~/LinEmb/qtApp/server/ # Chemin vers le projet QT (contenant le fichier .pro)

# Creation du makefile
~/LinEmb/build-pi4/host/bin/qmake server.pro

# Compilation
make

#Envoie sur le RPI
ssh root@157.26.91.83 "mkdir -p /home/rpi/apps"
scp ./server root@157.26.91.83:/home/rpi/apps/

# Sur le raspberry

## Optionel, si le driver n'est pas démarré (exemple, le raspberry vien d'être allumé)
modprobe -r drvTestI2C	// Pour le retirer
modprobe drvTestI2C // Pour le recharger

## Executer le programme sur le RPI
cd /home/rpi/apps
./server -platform vnc -geometry 500x500 -plugin evdevmouse &
```

> :warning: Pour éviter tout problème avec les threads, il ne faut pas arrêter le serveur en effectuant un "CTRL C", qui tue le serveur sans arrêter le thread.
> Il faut passer par **Remmina** et cliquer sur le bouton **quitter**, ce qui arrêtera correctement le serveur. :warning:

## Spécificité du serveur
Dans notre cas, l'adresse du serveur est `157.26.91.83`, et écoute sur le port `50886`.

Coté code, quelques remarques :
- Le serveur peut fonctionner aussi bien sur un environnement QT5 que QT6. Etant donné que des différences mineures sont présentes entre les deux versions. Pour une compilation sous Qt6, il faut décommenter le *define* `COMPILE_QT6` en ligne 4 du `server.h`. Pour Qt5, il faut que le *define* soit commenté.
- Le serveur peut être configurer pour lire les fichiers de tests du driver plutôt que le driver lui-même. Pour lire les fichiers de tests, il faut décommenter le *define* `USE_TEST_FILE`en ligne 5 du `server.h`.<br>:warning: Le chemin des fichiers de test est **hard-codé** en ligne 43.<br>Les fichiers de tests sont des "données générées" par un script python qui "simule" ce que la lecture du driver produirait. Ils se trouvent dans le dossier `gen-pseudo-data`).
- Le serveur va lire périodiquement les données du driver. Ce temps est défini dans `server.h` en ligne 49.

### Problème rencontré
L'envoie des données du serveur vers le client ne peut pas être fait depuis un thread (`QFuture`) (le serveur "envoie" les données, mais le client ne les reçoit jamais). 

Les corrections qui ont du être faites sont :

**server.h**
Ajout d'un signal pour retransmettre les données à envoyer
```c++
signals:
// Event pour transmettre l'obtention de données, sans ça, le thread ne peut pas envoyer les données aux clients
void eventNewDataGet(const quint16 r, const quint16 g, const quint16 b, const quint16 ir);
```

**server.cpp**
Connection sur signal, dans le constructeur
```c++
connect(this, &Server::eventNewDataGet, this, &Server::sendToAll);
```

Envoie des données depuis la méthode executée dans le thread
```c++
//sendToAll(r, g, b, ir); // Envoyer des donnes depuis un thread ne fonctionne pas (le client ne reçoit rien)
Q_EMIT eventNewDataGet(r, g, b, ir); // Solution : passer par un event qui est géré par le processus de base
```

## Spécificité du client
Le client est prévu pour tourner sur Windows, mais devrait sans problème pouvoir être compilé sur Linux.
Il utilise QtChart pour afficher les quatres graphiques.
