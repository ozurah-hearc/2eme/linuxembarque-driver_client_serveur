/***************************************************************************//**
*  \file       drvTestI2C.c
*
*  \details    Simple I2C driver explanation 
*
*  \author     FSA
*
* *******************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/kernel.h>
 
#define I2C_BUS_AVAILABLE   (          1 )              // I2C Bus available in our Raspberry Pi
#define SLAVE_DEVICE_NAME   ( "TCS34725" )              // Device and Driver Name
#define TCS_SLAVE_ADDR      (       0x29 )              // TCS34725 Slave Address
 
static struct i2c_adapter *tcs_i2c_adapter = NULL;  	// I2C Adapter Structure
static struct i2c_client  *tcs_i2c_client  = NULL;  	// I2C Cient Structure 
 
/*
** This function writes the data into the I2C client
**
**  Arguments:
**      buff -> buffer to be sent
**      len  -> Length of the data
**   
*/
static int I2C_Write(unsigned char *buf, unsigned int len)
{
    /*
    ** Sending Start condition, Slave address with R/W bit, 
    ** ACK/NACK and Stop condtions will be handled internally.
    */ 
    int ret = i2c_master_send(tcs_i2c_client, buf, len);
    
    return ret;
}
 
/*
** This function reads one byte of the data from the I2C client
**
**  Arguments:
**      out_buff -> buffer wherer the data to be copied
**      len      -> Length of the data to be read
** 
*/
static int I2C_Read(unsigned char *out_buf, unsigned int len)
{
    /*
    ** Sending Start condition, Slave address with R/W bit, 
    ** ACK/NACK and Stop condtions will be handled internally.
    */ 
    int ret = i2c_master_recv(tcs_i2c_client, out_buf, len);
    
    return ret;
}
 
/*
** This function sends the command/data to the TCS.
**
**  Arguments:
**      data   -> data to be written
** 
*/
static void TCS_Write(unsigned char * data)
{
    int ret;
    
    ret = I2C_Write(data, 2);
}

/*
** This function reads the data to the TCS.
**
**  Arguments:
**      data   -> data to be read
** 
*/
static void TCS_Read(unsigned char * data)
{
    int ret;
  	// Read 8 bytes of data from register(0x94)
	// cData lsb, cData msb, red lsb, red msb, green lsb, green msb, blue lsb, blue msb
	char reg[1] = {0x94};
	  
	ret = I2C_Write(reg, 1);  
    ret = I2C_Read(data, 8);
}
  
/*
** This function sends the commands that need to used to Initialize the TCS.
**
**  Arguments:
**      none
** 
*/

static int TCS_init(void)
{
	char config[2] = {0};
	char data[8] = {0};
	int cData;
	int red, green, blue;

    msleep(10);               // delay
 
    //
    // Commands to initialize the TCS
    //
    
    // Select enable register(0x80)
	// Power ON, RGBC enable, wait time disable(0x03)
	config[0] = 0x80;
	config[1] = 0x03;
	TCS_Write(config);
	// Select ALS time register(0x81)
	// Atime = 700 ms(0x00)
	config[0] = 0x81;
	config[1] = 0x00;
	TCS_Write(config);
	// Select Wait Time register(0x83)
	// WTIME : 2.4ms(0xFF)
	config[0] = 0x83;
	config[1] = 0xFF;
	TCS_Write(config);
	// Select control register(0x8F)
	// AGAIN = 1x(0x00)
	config[0] = 0x8F;
	config[1] = 0x00;
	TCS_Write(config);
	msleep(10);
	
	TCS_Read(data);
	// Convert the data
	cData = (data[1] * 256 + data[0]);
	red = (data[3] * 256 + data[2]);
	green = (data[5] * 256 + data[4]);
	blue = (data[7] * 256 + data[6]);

	// Output data to screen
	pr_info("Red color luminance : %d lux \n", red);
	pr_info("Green color luminance : %d lux \n", green);
	pr_info("Blue color luminance : %d lux \n", blue);
	pr_info("IR  luminance : %d lux \n", cData);

    return 0;
}
 
/*
** This function getting called when the slave has been found
** Note : This will be called only once when we load the driver.
*/
static int tcs_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    pr_info("TCS Probed!!!\n");  
    return 0;
}
 
/*
** This function getting called when the slave has been removed
** Note : This will be called only once when we unload the driver.
*/
static int tcs_remove(struct i2c_client *client)
{    
    pr_info("TCS Removed!!!\n");
    return 0;
}
 
/*
** Structure that has slave device id
*/
static const struct i2c_device_id tcs_id[] = {
        { SLAVE_DEVICE_NAME, 0 },
        { }
};
MODULE_DEVICE_TABLE(i2c, tcs_id);
 
/*
** I2C driver Structure that has to be added to linux
*/
static struct i2c_driver tcs_driver = {
        .driver = {
            .name   = SLAVE_DEVICE_NAME,
            .owner  = THIS_MODULE,
        },
        .probe          = tcs_probe,
        .remove         = tcs_remove,
        .id_table       = tcs_id,
};
 
/*
** I2C Board Info strucutre
*/
static struct i2c_board_info tcs_i2c_board_info = {
        I2C_BOARD_INFO(SLAVE_DEVICE_NAME, TCS_SLAVE_ADDR)
    };
 
/*
** Module Init function
*/
static int __init tcs_driver_init(void)
{
    int ret = -1;
    tcs_i2c_adapter     = i2c_get_adapter(I2C_BUS_AVAILABLE);
    
    if( tcs_i2c_adapter != NULL )
    {
        tcs_i2c_client = i2c_new_client_device(tcs_i2c_adapter, &tcs_i2c_board_info);      
        if( tcs_i2c_client != NULL )
        {
            i2c_add_driver(&tcs_driver);
            ret = 0;
        }
        
        i2c_put_adapter(tcs_i2c_adapter);
    }
    TCS_init();
    pr_info("Driver Added!!!\n");
    return ret;
}
 
/*
** Module Exit function
*/
static void __exit tcs_driver_exit(void)
{
    i2c_unregister_device(tcs_i2c_client);
    i2c_del_driver(&tcs_driver);
    pr_info("Driver Removed!!!\n");
}
 
module_init(tcs_driver_init);
module_exit(tcs_driver_exit);
 
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("FSA <blabla@gmail.com>");
MODULE_DESCRIPTION("Simple I2C driver for TCS34725");
MODULE_VERSION("1.0");
