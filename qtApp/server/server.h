#ifndef HEADER_SERVER
#define HEADER_SERVER

//#define COMPILE_QT6
//#define USE_TEST_FILE // Penser a changer le chemin absolu du dossier de test

#include <QWidget>
#include <qtconcurrentrun.h>

class QLabel;
class QPushButton;
class QTcpServer;
class QTcpSocket;

class Server : public QWidget
{
    Q_OBJECT

private:

#define BUFFER_LENGTH (8 + 100*8)               // Taille du buffer du driver

    QLabel *lbStatus;
    QPushButton *btnQuit; // Pour terminer la connexion du serveur
    QTcpServer *tcpServer; // Connection TCP
    QList<QTcpSocket *> socketList; // Liste des clients

    // Données obtenues
    QList<quint16> red;
    QList<quint16> green;
    QList<quint16> blue;
    QList<quint16> infraRed;

    // Thread pour l'execution de la lecture du driver et la transmission des données
    QFuture<void> future;

    const quint64 newValuesSizeLimit = 100; // Le driver retourne au max 100 valeurs
    quint64 currentCounter = 0; // Index du nombre de valeur obtenue sur le driver (index fournis par le driver)

    std::string path = "/dev/drvTest";

#ifdef USE_TEST_FILE
    std::string testPath = "C:/Users/jonas.allemann/Desktop/- Bureau -/2eme SP/Linux embarquee/Projet/gr11/gen-pseudo-data/drvTest";
    int nbTestFile = 19; // Nombre de fichier de test (les nombres vont de 0 à n inclus, et doivent se suivre)
    int tmpValIndex = 0; // Index du fichier de test qui est lu
#endif

    QList<QList<quint16>> getFromDriver(); // 1er Qlist = size fixe de 4 R;G;B;IR
    int refreshFreqMs = 2000; // Tout les combien de temps le driver est lu

    // action executée en boucle par le thread : lis le driver, et récupère les données de ce dernier
    void constantUpdate();

public:
    explicit Server(QWidget* parent=nullptr);
    virtual ~Server();
    // Envoie des données aux clients connectés
    void sendToAll(const quint16 r, const quint16 g, const quint16 b, const quint16 ir);

private slots:
    // Nouveau client qui se connecte
    void newConnection();
    // Client qui se déconnecte
    void socketDisconnected();

signals:
    // Event pour transmettre l'obtention de données, sans ça, le thread ne peux pas envoyer les données aux clients
    void eventNewDataGet(const quint16 r, const quint16 g, const quint16 b, const quint16 ir);
};

#endif
