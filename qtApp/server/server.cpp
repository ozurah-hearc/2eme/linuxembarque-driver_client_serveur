#include <QtWidgets>
#include <QtNetwork>
#include <QtConcurrent/QtConcurrent>
#include <qtconcurrentrun.h>
#include <QDebug>
#include <QtEndian>
#include "server.h"
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

Server::Server(QWidget* parent) : QWidget(parent)
{

    // Création et disposition des widgets de la fenêtre
    lbStatus = new QLabel(this);
    btnQuit = new QPushButton(tr("Quitter"), this);
    connect(btnQuit, &QPushButton::clicked, qApp, &QApplication::quit);
    QVBoxLayout *layout = new QVBoxLayout;

    layout->addWidget(lbStatus);
    layout->addWidget(btnQuit);

    setLayout(layout);

    setWindowTitle(tr("Chat - Serveur"));

    // Démarrage du serveur sur toutes les IP disponibles et sur le port 50886
    QString status = "";
    tcpServer = new QTcpServer(this);
    if (!tcpServer->listen(QHostAddress::Any, 50886))
    {
        status = tr("Le serveur n'a pas pu être démarré. Raison :<br />") + tcpServer->errorString();

        qDebug() << status;

        lbStatus->setText(status);
    }
    else
    {
        status = tr("Le serveur <strong>")+tcpServer->serverAddress().toString() + tr("</strong> a été démarré sur le port <strong>") + QString::number(tcpServer->serverPort()) + tr("</strong>.<br />Des clients peuvent maintenant se connecter.");

        qDebug() << status;
        lbStatus->setText(status);

        connect(this, &Server::eventNewDataGet, this, &Server::sendToAll);
        connect(tcpServer, &QTcpServer::newConnection, this, &Server::newConnection);
    }

#ifdef COMPILE_QT6
    future = QtConcurrent::run(&Server::constantUpdate, this);
#else
    future = QtConcurrent::run(this, &Server::constantUpdate);
#endif
}

Server::~Server()
{
    qDebug() << "En attante d'arret du thread";
    future.cancel();
    future.waitForFinished();
    qDebug() << "Le thread c'est correctement temine, le serveur est destruit.";
}

QList<QList<quint16>> Server::getFromDriver()
{
    // ***** RESULT VARIABLE ****
#ifdef COMPILE_QT6
    QList<QList<quint16>> newLux(4);
#else
    QList<QList<quint16>> newLux;
    newLux.append(QList<quint16>());
    newLux.append(QList<quint16>());
    newLux.append(QList<quint16>());
    newLux.append(QList<quint16>());
#endif

    // ***** READING DRIVER *****
    char receive[BUFFER_LENGTH];
    int ret, fd;

    qDebug() << "Starting read driver ...";

#ifdef USE_TEST_FILE
    path = testPath;
    path += std::to_string(tmpValIndex);
#endif

    fd = open(path.c_str(), O_RDWR); // Open the device with read/write access
    if (fd < 0)
    {
        perror("Failed to open the device...");
        return newLux;
    }

    qDebug() << "Reading from the device...\n";
    ret = read(fd, receive, BUFFER_LENGTH); // Read the response from the driver
    if (ret < 0)
    {
        perror("Failed to read the message from the device.");
        return newLux;
    }

    // ********* RESULT *********
    quint64 counter = qFromLittleEndian<quint64>(receive);

    quint64 nbNewValues = qMin(counter - currentCounter, newValuesSizeLimit);
    //qDebug() << counter << "->>>>> "<< currentCounter << "=" << nbNewValues;
    currentCounter = counter;

    qDebug() << "number of data send by the driver : " << counter;

    for (quint64 i = newValuesSizeLimit - nbNewValues; i < newValuesSizeLimit; i++)
    {
        size_t offset = 8 + 8 * i;
        quint16 red = qFromLittleEndian<quint16>(receive + offset + 0);
        quint16 green = qFromLittleEndian<quint16>(receive + offset + 2);
        quint16 blue = qFromLittleEndian<quint16>(receive + offset + 4);
        quint16 ir = qFromLittleEndian<quint16>(receive + offset + 6);

        //qDebug() << counter << " --> " << red << " / " << green << " / " << blue << " / " << ir;

        newLux[0].append(red);
        newLux[1].append(green);
        newLux[2].append(blue);
        newLux[3].append(ir);
    }

    qDebug() << "Ending read driver ...";
    return newLux;
}

void Server::newConnection()
{
    QTcpSocket *newClient = tcpServer->nextPendingConnection();
    socketList << newClient;

    connect(newClient, &QTcpSocket::disconnected, this, &Server::socketDisconnected);
}

void Server::socketDisconnected()
{
    // On détermine quel client se déconnecte
    // Si par hasard on n'a pas trouvé le client à l'origine du signal, on arrête la méthode

    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

    if (socket == nullptr)
    {
        return;
    }

    socketList.removeOne(socket);
    socket->deleteLater();
}

void Server::sendToAll(const quint16 r, const quint16 g, const quint16 b, const quint16 ir)
{
    // Conservation de l'historique des données obtenue, pourrait être utile pour du debug ou du logging
    red.append(r);
    green.append(g);
    blue.append(b);
    infraRed.append(ir);

    // Préparation du paquet
    QByteArray data;
    QDataStream byteArrayStream(&data, QIODevice::WriteOnly);

    byteArrayStream << r;
    byteArrayStream << b;
    byteArrayStream << g;
    byteArrayStream << ir;

    // Envoi du paquet préparé à tous les clients connectés au serveur

    foreach(QTcpSocket* socket, socketList)
    {
        qDebug() << "sended " << data;
        socket->write(data);
    }
}

void Server::constantUpdate()
{
    qDebug() << ":)";
    while(true && !future.isCanceled())
    {

#ifdef USE_TEST_FILE
        tmpValIndex++;
        if (tmpValIndex > nbTestFile)
        {
            tmpValIndex = 0;
            currentCounter = 0; // On relit les même fichiers, on retourne donc au compteur 0 pour effectuer recevoir les données deja obtenus
        }
#endif

        QList<QList<quint16>> newLux = getFromDriver();

        int max = qMax(newLux[0].size(), newLux[1].size());
        max = qMax(max, newLux[2].size());
        max = qMax(max, newLux[3].size());

        qDebug() << "Number of new values = "<< max;

        for (int i = 0; i < max; i++)
        {
            quint16 r = i < newLux[0].size() ? newLux[0][i] : 0;
            quint16 g = i < newLux[1].size() ? newLux[1][i] : 0;
            quint16 b = i < newLux[2].size() ? newLux[2][i] : 0;
            quint16 ir = i < newLux[3].size() ? newLux[3][i] : 0;

            qDebug() << r << " // " << g << " // " << b << " // " << ir;

            //sendToAll(r, g, b, ir); // Envoyer des donnes depuis un thread ne fonctionne pas (le client ne reçois rien)
            Q_EMIT eventNewDataGet(r, g, b, ir); // Solution : passer par un event qui est gérer par le processus de base
        }

        QObject().thread()->usleep(1000*refreshFreqMs);
    }
}
