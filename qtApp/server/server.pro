QT += widgets network concurrent

TEMPLATE = app
#CONFIG += release
TARGET = server
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += \
    server.h
SOURCES += main.cpp \
    server.cpp
