#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QLineSeries>
#include <QChartView>

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QTcpSocket* socket;
    QLineEdit* ipField;
    QSpinBox* portField;
    QPushButton* connectButton;
    QLineSeries* redSerie;
    QLineSeries* greenSerie;
    QLineSeries* blueSerie;
    QLineSeries* irSerie;
    QChartView* redView;
    QChartView* greenView;
    QChartView* blueView;
    QChartView* irView;
    QChart* chart;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void setupTCP();
    void setupUI();
    void connectTCP();
    QChartView* buildChart(QString title, QColor color, QLineSeries* series);


private slots:
    void receiveData();
    void connected();
    void disconnected();
    void processSocketError(QAbstractSocket::SocketError error);
};
#endif // MAINWINDOW_H
