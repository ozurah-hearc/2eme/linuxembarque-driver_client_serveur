#include "mainwindow.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QGroupBox>
#include <QtCharts>
#include <QLineSeries>
#include <QChartView>

// 157.26.91.83

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setupTCP();
    this->setupUI();
}

MainWindow::~MainWindow()
{
}

void MainWindow::setupTCP()
{
    qDebug() << "SETUP TCP";
    this->socket = new QTcpSocket(this);

    connect(this->socket, &QTcpSocket::connected, this, &MainWindow::connected);
    connect(this->socket, &QTcpSocket::readyRead, this, &MainWindow::receiveData);
    connect(this->socket, &QTcpSocket::disconnected, this, &MainWindow::disconnected);
    connect(this->socket, &QTcpSocket::errorOccurred, this, &MainWindow::processSocketError);

}

void MainWindow::setupUI()
{
    QVBoxLayout* layout = new QVBoxLayout();

    // build connection part
    QHBoxLayout* connectLayout = new QHBoxLayout();

    this->ipField = new QLineEdit("157.26.107.23", this);

    this->portField = new QSpinBox(this);
    this->portField->setMinimum(0);
    this->portField->setMaximum(65535);
    this->portField->setValue(50886);

    this->connectButton = new QPushButton("Connect", this);

    connectLayout->addWidget(this->ipField, 3);
    connectLayout->addWidget(this->portField, 1);
    connectLayout->addWidget(this->connectButton, 2);

    QGroupBox* groupBox = new QGroupBox("Server datas");
    groupBox->setLayout(connectLayout);
    layout->addWidget(groupBox);

    // build chart part
    QGridLayout* gridLayout = new QGridLayout();

    this->redSerie = new QLineSeries();
    this->greenSerie = new QLineSeries();
    this->blueSerie = new QLineSeries();
    this->irSerie = new QLineSeries();

    this->redView = this->buildChart("Red", QColorConstants::Red, this->redSerie);
    this->greenView = this->buildChart("Green", QColorConstants::Green, this->greenSerie);
    this->blueView = this->buildChart("Blue", QColorConstants::Blue, this->blueSerie);
    this->irView = this->buildChart("IR", QColorConstants::Black, this->irSerie);

    gridLayout->addWidget(this->redView, 0, 0);
    gridLayout->addWidget(this->greenView, 1, 0);
    gridLayout->addWidget(this->blueView, 0, 1);
    gridLayout->addWidget(this->irView, 1, 1);
    layout->addLayout(gridLayout);

    // Set global layout to window
    QWidget *window = new QWidget();
    window->setLayout(layout);
    setCentralWidget(window);

    connect(this->connectButton, &QPushButton::clicked, this, &MainWindow::connectTCP);

}

void MainWindow::connectTCP()
{
    qDebug() << "CONNECT TCP";
    this->connectButton->setEnabled(false);
    //this->socket->abort();
    this->socket->connectToHost(this->ipField->text(), static_cast<quint16>(this->portField->value()));
}

QChartView* MainWindow::buildChart(QString title, QColor color, QLineSeries* series)
{
    series->setColor(color);

    QChart* chart = new QChart();
    chart->legend()->hide();
    chart->createDefaultAxes();
    chart->setTitle(title);
    chart->removeAllSeries();
    chart->addSeries(series);

    QValueAxis* axisX = new QValueAxis;
    axisX->setMin(0);
    axisX->setMax(20);
    chart->addAxis(axisX, Qt::AlignLeft);
    series->attachAxis(axisX);
    QValueAxis* axisY = new QValueAxis;
    axisY->setMin(0);
    axisY->setMax(20);
    chart->addAxis(axisY, Qt::AlignBottom);
    series->attachAxis(axisY);

    QChartView* chartView = new QChartView(chart, this);
    chartView->setRenderHint(QPainter::Antialiasing);
    return chartView;
}

void MainWindow::receiveData()
{
    qDebug() << "RECEIVE";
    QDataStream in(this->socket);
    while(!in.atEnd())
    {

        qDebug() << "DATA";

        qint16 r;
        qint16 g;
        qint16 b;
        qint16 ir;

        in >> r;
        in >> g;
        in >> b;
        in >> ir;

        int maxRed = 0;
        int maxGreen = 0;
        int maxBlue = 0;
        int maxIR = 0;
        for(int i = 0; i < this->redSerie->count(); i++)
        {
            int scanRed = this->redSerie->at(i).y();
            if(maxRed < scanRed)
            {
                maxRed = scanRed;
            }
        }
        for(int i = 0; i < this->greenSerie->count(); i++)
        {
            int scanGreen = this->greenSerie->at(i).y();
            if(maxGreen < scanGreen)
            {
                maxGreen = scanGreen;
            }
        }
        for(int i = 0; i < this->blueSerie->count(); i++)
        {
            int scanBlue = this->blueSerie->at(i).y();
            if(maxBlue < scanBlue)
            {
                maxBlue = scanBlue;
            }
        }
        for(int i = 0; i < this->irSerie->count(); i++)
        {
            int scanIR = this->irSerie->at(i).y();
            if(maxIR < scanIR)
            {
                maxIR = scanIR;
            }
        }


        this->redSerie->append(this->redSerie->count(), r);
        this->redView->chart()->axes(Qt::Horizontal, this->redSerie).first()->setMax(this->redSerie->count());
        this->redView->chart()->axes(Qt::Vertical, this->redSerie).first()->setMax(2*maxRed);
        this->redView->chart()->plotArea();
        this->greenSerie->append(this->greenSerie->count(), g);
        this->greenView->chart()->axes(Qt::Horizontal, this->greenSerie).first()->setMax(this->greenSerie->count());
        this->greenView->chart()->axes(Qt::Vertical, this->greenSerie).first()->setMax(2*maxGreen);
        this->greenView->chart()->plotArea();
        this->blueSerie->append(this->blueSerie->count(), b);
        this->blueView->chart()->axes(Qt::Horizontal, this->blueSerie).first()->setMax(this->blueSerie->count());
        this->blueView->chart()->axes(Qt::Vertical, this->blueSerie).first()->setMax(2*maxBlue);
        this->blueView->chart()->plotArea();
        this->irSerie->append(this->irSerie->count(), ir);
        this->irView->chart()->axes(Qt::Horizontal, this->irSerie).first()->setMax(this->irSerie->count());
        this->irView->chart()->axes(Qt::Vertical, this->irSerie).first()->setMax(2*maxIR);
        this->irView->chart()->plotArea();
        this->repaint();
    }
}

void MainWindow::connected()
{
    qDebug() << "CONNECTED";
    //this->connectButton->setEnabled(false);
}

void MainWindow::disconnected()
{
    qDebug() << "DISCONNECTED";
    this->connectButton->setEnabled(true);
}

void MainWindow::processSocketError(QAbstractSocket::SocketError error)
{
    this->connectButton->setEnabled(true);

    switch(error)
    {
        case QAbstractSocket::HostNotFoundError:
            QMessageBox::critical(this, "Socket error", "Host not found");
            break;
        case QAbstractSocket::ConnectionRefusedError:
            QMessageBox::critical(this, "Socket error", "Connection refused");
            break;
        case QAbstractSocket::RemoteHostClosedError:
            QMessageBox::critical(this, "Socket error", "Remote host closed connection");
            break;
        default:
            QMessageBox::critical(this, "Socket error", "Unknown error");
            break;
    }

    this->connectButton->setEnabled(true);
}

