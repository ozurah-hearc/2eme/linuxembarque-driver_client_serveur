QT += widgets network
TEMPLATE = app
CONFIG += release
TARGET = S15e1-client
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += \
    chatclient.h
FORMS += \
    chatclient.ui
SOURCES += main.cpp \
    chatclient.cpp

RESOURCES += \
    resources.qrc
