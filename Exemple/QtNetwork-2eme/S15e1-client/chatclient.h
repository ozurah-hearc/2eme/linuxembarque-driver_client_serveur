#ifndef HEADER_CHATCLIENT
#define HEADER_CHATCLIENT

#include <QWidget>
#include <QAbstractSocket>
#include "ui_chatclient.h"

class QTcpSocket;

class ChatClient : public QWidget, private Ui::ChatClient
{
    Q_OBJECT

private:
    QTcpSocket *socket;
    quint16 messageSize;

public:
    explicit ChatClient(QWidget* parent=nullptr);

private slots:
    void on_btnConnect_clicked();
    void on_btnSend_clicked();
    void on_fldMessage_returnPressed();
    void receiveData();
    void connected();
    void disconnected();
    void processSocketError(QAbstractSocket::SocketError error);

protected:
    void closeEvent(QCloseEvent *) override;
};

#endif
