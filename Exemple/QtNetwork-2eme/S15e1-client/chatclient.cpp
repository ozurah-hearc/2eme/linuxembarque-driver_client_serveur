#include <QtWidgets>
#include <QtNetwork>
#include "chatclient.h"

ChatClient::ChatClient(QWidget *parent) : QWidget(parent)
{
    setupUi(this);

    messageSize = 0;

    socket = new QTcpSocket(this);
    connect(socket, &QTcpSocket::readyRead, this, &ChatClient::receiveData);
    connect(socket, &QTcpSocket::connected, this, &ChatClient::connected);
    connect(socket, &QTcpSocket::disconnected, this, &ChatClient::disconnected);
    connect(socket, &QTcpSocket::errorOccurred, this, &ChatClient::processSocketError);
}

void ChatClient::on_btnConnect_clicked()
{
    fldListMessages->append(tr("<em>Tentative de connexion en cours...</em>"));
    btnConnect->setEnabled(false);

    // Désactive les connexions précédentes s'il y en a et connection au serveur
    socket->abort();
    socket->connectToHost(fldIPAddress->text(), static_cast<quint16>(fldTCPPort->value()));
}

void ChatClient::on_btnSend_clicked()
{
    // On prépare le paquet à envoyer
    quint16 messageSize;
    QByteArray data;
    QDataStream byteArrayStream(&data, QIODevice::WriteOnly);

    QString message = QString(tr("<strong>%1</strong> : %2").arg(fldNickname->text()).arg(fldMessage->text()));
    messageSize = static_cast<quint16>(message.size());

    byteArrayStream << messageSize;
    byteArrayStream << message;

    // On envoie le paquet
    socket->write(data);
    messageSize = 0;

    // On vide la zone d'écriture du message et on remet le curseur à l'intérieur
    fldMessage->clear();
    fldMessage->setFocus();
}

// Appuyer sur la touche Entrée a le même effet que cliquer sur le bouton "Envoyer"
void ChatClient::on_fldMessage_returnPressed()
{
    on_btnSend_clicked();
}

// On a reçu un paquet (ou un sous-paquet)
void ChatClient::receiveData()
{
    /* Même principe que lorsque le serveur reçoit un paquet :
    On essaie de récupérer la taille du message
    Une fois qu'on l'a, on attend d'avoir reçu le message entier (en se basant sur la taille annoncée messageSize)
    */
    QDataStream in(socket);
    if (messageSize == 0)
    {
        if (socket->bytesAvailable() < sizeof(qint16))
        {
             return;
        }

        in >> messageSize;
    }

    if (socket->bytesAvailable() < messageSize)
    {
        return;
    }

    // Si on arrive jusqu'à cette ligne, on peut récupérer le message entier
    QString message;
    in >> message;

    // On affiche le message sur la zone de Chat
    fldListMessages->append(message);

    // On remet la taille du message à 0 pour pouvoir recevoir de futurs messages
    messageSize = 0;
}

// Ce slot est appelé lorsque la connexion au serveur a réussi
void ChatClient::connected()
{
    fldListMessages->append(tr("<em>Connexion réussie !</em>"));
    btnConnect->setEnabled(true);
}

// Ce slot est appelé lorsqu'on est déconnecté du serveur
void ChatClient::disconnected()
{
    fldListMessages->append(tr("<em>Déconnecté du serveur</em>"));
}

// Ce slot est appelé lorsqu'il y a une erreur
void ChatClient::processSocketError(QAbstractSocket::SocketError error)
{
    switch(error)
    {
        case QAbstractSocket::HostNotFoundError:
            fldListMessages->append(tr("<em>ERREUR : le serveur n'a pas pu être trouvé. Vérifiez l'IP et le port.</em>"));
            break;
        case QAbstractSocket::ConnectionRefusedError:
            fldListMessages->append(tr("<em>ERREUR : le serveur a refusé la connexion. Vérifiez si le programme \"serveur\" a bien été lancé. Vérifiez aussi l'IP et le port.</em>"));
            break;
        case QAbstractSocket::RemoteHostClosedError:
            fldListMessages->append(tr("<em>ERREUR : le serveur a coupé la connexion.</em>"));
            break;
        default:
            fldListMessages->append(tr("<em>ERREUR : ") + socket->errorString() + tr("</em>"));
    }

    btnConnect->setEnabled(true);
}

void ChatClient::closeEvent(QCloseEvent *)
{
    // Déconnecte proprement
    // Aussi appelé par le destucteur de QTcpSocket, mais dans ce cas, le signal disconnected est émis et le slot de cette classe pourraît être appelé
    socket->disconnectFromHost();
}
