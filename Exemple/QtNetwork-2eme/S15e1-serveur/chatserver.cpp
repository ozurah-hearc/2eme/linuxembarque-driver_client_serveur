#include <QtWidgets>
#include <QtNetwork>
#include "chatserver.h"
#include <QDebug>

ChatServer::ChatServer(QWidget* parent) : QWidget(parent)
{
    // Création et disposition des widgets de la fenêtre
    lbStatus = new QLabel(this);
    btnQuit = new QPushButton(tr("Quitter"), this);
    connect(btnQuit, &QPushButton::clicked, qApp, &QApplication::quit);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(lbStatus);
    layout->addWidget(btnQuit);
    setLayout(layout);

    setWindowTitle(tr("Chat - Serveur"));

    // Démarrage du serveur sur toutes les IP disponibles et sur le port 50585
    tcpServer = new QTcpServer(this);
    if (!tcpServer->listen(QHostAddress::Any, 50885))
    {
        lbStatus->setText(tr("Le serveur n'a pas pu être démarré. Raison :<br />") + tcpServer->errorString());
    }
    else
    {
        lbStatus->setText(tr("Le serveur a été démarré sur le port <strong>") + QString::number(tcpServer->serverPort()) + tr("</strong>.<br />Des clients peuvent maintenant se connecter."));
        connect(tcpServer, &QTcpServer::newConnection, this, &ChatServer::newConnection);
    }

    messageSize = 0;
}

void ChatServer::newConnection()
{
    sendToAll(tr("<em>Un nouveau client vient de se connecter</em>"));

    QTcpSocket *newClient = tcpServer->nextPendingConnection();
    socketList << newClient;

    connect(newClient, &QTcpSocket::readyRead, this, &ChatServer::receiveData);
    connect(newClient, &QTcpSocket::disconnected, this, &ChatServer::socketDisconnected);
}

void ChatServer::receiveData()
{
    // static int loop = 0;
    // loop++;
    // qDebug() << "donneesRecues : " << loop;

    // On reçoit un paquet (ou un sous-paquet) d'un des clients
    // On détermine quel client envoie le message (recherche du QTcpSocket du client)
    // Si par hasard on n'a pas trouvé le client à l'origine du signal, on arrête la méthode
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == nullptr)
    {
        return;
    }

    QDataStream in(socket);

    // Si on ne connaît pas encore la taille du message, on essaie de la récupérer
    if (messageSize == 0)
    {
        // Si on n'a pas reçu la taille du message en entier, on arrête la méthode en attendant plus de données
        if (socket->bytesAvailable() < sizeof(qint16))
        {
            return;
        }

        // On a reçu la taille du message en entier: On la récupère
        in >> messageSize;
    }

    // On connaît la taille du message: On vérifie si on a reçu le message en entier.
    // Si on n'a pas encore tout reçu, on arrête la méthode en attendant plus de données
    if (socket->bytesAvailable() < messageSize)
    {
        return;
    }

    // On a reçu tout le message: On le récupère
    QString message;
    in >> message;

    // qDebug() << message;
    // On renvoie le message à tous les clients
    sendToAll(message);

    // Remise de la taille du message à 0 pour la réception des futurs messages
    messageSize = 0;
}

void ChatServer::socketDisconnected()
{
    sendToAll(tr("<em>Un client vient de se déconnecter</em>"));

    // On détermine quel client se déconnecte
    // Si par hasard on n'a pas trouvé le client à l'origine du signal, on arrête la méthode
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == nullptr)
    {
        return;
    }
    socketList.removeOne(socket);

    socket->deleteLater();
}

void ChatServer::sendToAll(const QString &message)
{
    // Préparation du paquet
    quint16 messageSize = 0;
    QByteArray data;
    QDataStream byteArrayStream(&data, QIODevice::WriteOnly);

    byteArrayStream << messageSize; // On écrit 0 au début du paquet pour réserver la place pour écrire la taille
    byteArrayStream << message;     // On ajoute le message à la suite
    byteArrayStream.device()->seek(0); // On se replace au début du paquet
    messageSize = static_cast<quint16>(data.size()) - sizeof(quint16);
    byteArrayStream << messageSize; // On écrase le 0 qu'on avait réservé par la longueur du message

    // Variante si la taille du packet peut être connue à l'avance
    // byteArrayStream << static_cast<quint16>(message.size());
    // byteArrayStream << message;

    // Envoi du paquet préparé à tous les clients connectés au serveur
    foreach(QTcpSocket* socket, socketList)
    {
        socket->write(data);
    }
}
