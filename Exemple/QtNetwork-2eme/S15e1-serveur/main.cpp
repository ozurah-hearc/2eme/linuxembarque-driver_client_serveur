#include <QApplication>
#include "chatserver.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    ChatServer window;
    window.show();

    return app.exec();
}
