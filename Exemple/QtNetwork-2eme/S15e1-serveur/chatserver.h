#ifndef HEADER_CHATSERVER
#define HEADER_CHATSERVER

#include <QWidget>

class QLabel;
class QPushButton;
class QTcpServer;
class QTcpSocket;

class ChatServer : public QWidget
{
    Q_OBJECT

private:
    QLabel *lbStatus;
    QPushButton *btnQuit;
    QTcpServer *tcpServer;
    QList<QTcpSocket *> socketList;
    quint16 messageSize;

public:
    explicit ChatServer(QWidget* parent=nullptr);
    void sendToAll(const QString &message);

private slots:
    void newConnection();
    void receiveData();
    void socketDisconnected();
};

#endif
