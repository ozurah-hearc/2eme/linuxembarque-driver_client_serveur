QT += widgets network
TEMPLATE = app
CONFIG += release
TARGET = S15e1-server
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += \
    chatserver.h
SOURCES += main.cpp \
    chatserver.cpp
